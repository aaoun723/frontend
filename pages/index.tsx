import Search from '../src/sections/search';
import HomeLayout from '../src/layouts/home';

const IndexPage = () => (
    <HomeLayout>
      <Search />
    </HomeLayout>
);

export default IndexPage;