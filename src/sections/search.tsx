import {
    Heading,
    HStack,
    VStack,
    SimpleGrid,
    GridItem,
    FormControl,
    FormLabel,
    Input,
    Select,
    Checkbox,
    Button,
    useColorMode,
    useColorModeValue,
    useBreakpointValue
  } from '@chakra-ui/react'
  
  const Search = () => {
    const { toggleColorMode } = useColorMode()
    const bgColor = useColorModeValue('gray.50', 'whiteAlpha.50')
    const secondaryTextColor = useColorModeValue('gray.600', 'gray.400')
    const colSpan = useBreakpointValue({ base: 2, md: 1 });

    return (
      <VStack
        w="full"
        h="full"
        p={10}
        spacing={6}
        bg={bgColor}
      >
        <VStack alignContent="flex-start" spacing={3}>
          <Heading size="2xl">ZDayPhisher</Heading>
            <Button onClick={toggleColorMode} variant="link" colorScheme="black">
              try changing the theme
            </Button>
        </VStack>
        <SimpleGrid columns={2} columnGap={3} rowGap={6} w="full">
          <GridItem colSpan={colSpan}>
            <FormControl>
              <FormLabel>Domain</FormLabel>
              <Input placeholder="binance.com" />
            </FormControl>
          </GridItem>
          <GridItem colSpan={colSpan}>
            <FormControl>
              <FormLabel>Pipeline</FormLabel>
              <Select>
                <option value="1">1 (DNS Twist, Syn Scanner, HTTProbe, Asset Scraper)</option>
                <option value="2" disabled>2 (coming soon)</option>
              </Select>
            </FormControl>
          </GridItem>
          <GridItem colSpan={2}>
            <Button variant="primary" size="lg" w="full">
              Scan
            </Button>
          </GridItem>
        </SimpleGrid>
      </VStack>
    )
  }
  
  export default Search
  