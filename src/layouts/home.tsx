import { Flex } from '@chakra-ui/react'

import Header from '../sections/header'

const HomeLayout: React.FC = ({ children }) => {
  return (
      <Flex
        direction="column"
        maxW={{ xl: '1200px' }}
        h="100vh"
        m="0 auto"
        p="0"
      >
        <Header></Header>
        {children}
      </Flex>
  )
}

export default HomeLayout
